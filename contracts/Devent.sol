pragma solidity 0.5.11;

 contract devent {
    
     struct rules {
         uint id_rules;
         uint organizer;
       //  uint[] coorganisateur;
       //  uint[] revenuedistribution;
         uint tickets_sale_rev;
         uint256 total_tickets_sale_rev;
         string signed_or;
     }
     
    struct ProUser {
         uint ID_ProUser;
        string Companyname;
        string Companyemail;
        string  Identification;
        string CompanyAddress;
        string  CompanyArea;
        string CompanyBankAccount;
    //    string password;
        address ProWallet;
     }
    
     struct project {
         uint id_project; // ou bien on va travailler par wallet
         string  project_name;
         string  project_type;
         string  localization;
         uint start_date;
         uint end_date;
         bool ongoing_project;
         string project_description;
         string project_contact_information;
         uint tickets_total_supply;
     }
    
     struct User {
        uint ID_User;
        string Username;
        string email;
        string  country;
        uint posta_Code;
        string password;
        address userWallet;
       
     }
    
     struct Ticket {
        uint ID_Ticket;
        string TicketName;
        uint TicketPrice ;
        string TicketDescription;
        bool validity ;
        uint ticketCreator ;
        uint ticketOwner ;
        string options ;
        
     }
     
mapping(uint => ProUser) public ListProUser;

        uint[] public ListIDProUser;
     
    mapping (uint => User)  public ListeUser ;

     uint[] public ListeIDUser ; 
     
     
    mapping (uint => mapping (uint => project)) public ListeProjectUser;

     mapping (uint => uint) public NumberProjectUser ;
     
     
     
     mapping (uint => mapping (uint => Ticket)) public ListeUserTicket;

     mapping (uint => uint)  public NumberTickettUser ;
     
     
    mapping(uint=> mapping(uint => Ticket)) public ListTicketProject;
     
     mapping(uint => uint) public NumberTicketsProjects;
     
     
     mapping (uint => rules) public ListeRules ;
      
    uint[] public ListIDRules;
    
 
         uint256 public ID_UserIndice = 1;

    
    function CreateUser (
        string memory Username,
        string memory email,
        string  memory country,
        uint posta_Code,
        string memory password,
        address userWallet) public {
        ListeUser[ID_UserIndice].ID_User = ID_UserIndice;
        ListeUser[ID_UserIndice].Username = Username;
        ListeUser[ID_UserIndice].email = email;
        ListeUser[ID_UserIndice].country = country;
        ListeUser[ID_UserIndice].posta_Code = posta_Code;
        ListeUser[ID_UserIndice].password = password;
        ListeUser[ID_UserIndice].userWallet = userWallet;
        ListeIDUser.push(ID_UserIndice) ;
        ID_UserIndice++;
        }
        
        
      function  getUser (uint ID_User) view public returns ( string memory ,
        string memory ,
        string  memory ,
        uint ,
        string memory ,
        address ) {
          return(   
        ListeUser[ID_User].Username ,
        ListeUser[ID_User].email,
        ListeUser[ID_User].country ,
        ListeUser[ID_User].posta_Code ,
        ListeUser[ID_User].password ,
        ListeUser[ID_User].userWallet );
      }
        uint256 public ProjectIndice = 1;
        
            function CreateProject (
            
         uint ID_User,
         string memory project_name,
         string memory project_type,
         string memory localization,
         uint start_date,
         uint end_date,
         bool ongoing_project,
         string memory project_description,
         string memory project_contact_information,
         uint tickets_total_supply) public {
             ListeProjectUser[ID_User][ProjectIndice].id_project = ProjectIndice;
             ListeProjectUser[ID_User][ProjectIndice].project_name = project_name;
             ListeProjectUser[ID_User][ProjectIndice].project_type = project_type;
             ListeProjectUser[ID_User][ProjectIndice].localization = localization;
             ListeProjectUser[ID_User][ProjectIndice].start_date = start_date;
             ListeProjectUser[ID_User][ProjectIndice].end_date = end_date;
             ListeProjectUser[ID_User][ProjectIndice].ongoing_project = ongoing_project;
             ListeProjectUser[ID_User][ProjectIndice].project_description = project_description;
             ListeProjectUser[ID_User][ProjectIndice].project_contact_information = project_contact_information;
             ListeProjectUser[ID_User][ProjectIndice].tickets_total_supply = tickets_total_supply;
             NumberProjectUser[ID_User]=ProjectIndice;
             ProjectIndice++;
             

        }
        
        function  getproject (uint ID_User , uint idProject) view public returns ( 
     
         string memory ,
         string memory ,
      //   string memory ,
         uint ,
         uint ,
         bool ,
         string memory 
         ) {
          return(   
             ListeProjectUser[ID_User][idProject].project_name ,
             ListeProjectUser[ID_User][idProject].project_type ,
          //   ListeProjectUser[ID_User][ProjectIndice].localization ,
             ListeProjectUser[ID_User][idProject].start_date,
             ListeProjectUser[ID_User][idProject].end_date,
             ListeProjectUser[ID_User][idProject].ongoing_project ,
             ListeProjectUser[ID_User][idProject].project_description 
           );
      }
        
                uint256 public rulesIndice = 1;

           
    function CreateRules (
 
          uint id_project,
       
         uint organizer,
     
         uint tickets_sale_rev,
         uint256 total_tickets_sale_rev,
         string memory signed_or) public {
        ListeRules[id_project].id_rules = rulesIndice;
        ListeRules[id_project].organizer = organizer;
        ListeRules[id_project].tickets_sale_rev = tickets_sale_rev;
        ListeRules[id_project].total_tickets_sale_rev = total_tickets_sale_rev;
        ListeRules[id_project].signed_or = signed_or;

        ListIDRules.push(rulesIndice) ;
        ID_UserIndice++;
        }
        
            function  getRules (uint id_project) view public returns ( 
         uint ,
         uint ,
         uint ,
         uint256 ,
         string memory ) {
          return(   
             ListeRules[id_project].id_rules,
        ListeRules[id_project].organizer,
        ListeRules[id_project].tickets_sale_rev ,
        ListeRules[id_project].total_tickets_sale_rev ,
        ListeRules[id_project].signed_or);
      }
      
       uint public ID_ProUser = 1;

     function addProUser(
        string memory _Companyname,
        string memory _Companyemail,
        string memory  _Identification,
        string memory _CompanyAddress,
        string memory  _CompanyArea,
        string memory _CompanyBankAccount,
       // string memory _password,
        address _ProWallet
    ) public { 
        
        ListProUser[ID_ProUser].ID_ProUser = ID_ProUser;
        ListProUser[ID_ProUser].Companyname = _Companyname;
        ListProUser[ID_ProUser].Companyemail = _Companyemail;
        ListProUser[ID_ProUser].Identification = _Identification;
        ListProUser[ID_ProUser].CompanyAddress = _CompanyAddress;
        ListProUser[ID_ProUser].CompanyArea = _CompanyArea;
        ListProUser[ID_ProUser].CompanyBankAccount = _CompanyBankAccount;
       // ListProUser[ID_ProUser].password = _password;
        ListProUser[ID_ProUser].ProWallet = _ProWallet;
        
        ListIDProUser.push(ID_ProUser);
        
             ID_ProUser ++;
        
    
    }
    
    uint public ID_Ticket = 1;
    
      function addTicket(
      uint _ID_User,
        string memory _TicketName,
        uint _TicketPrice ,
        string memory _TicketDescription,
        bool _validity ,
        uint _ticketCreator ,
        uint _ticketOwner ,
        string memory _options 
    ) public { 
        
        ListeUserTicket[_ID_User][ID_Ticket].ID_Ticket = ID_Ticket;
        ListeUserTicket[_ID_User][ID_Ticket].TicketName = _TicketName;
        ListeUserTicket[_ID_User][ID_Ticket].TicketPrice = _TicketPrice;
        ListeUserTicket[_ID_User][ID_Ticket].TicketDescription = _TicketDescription;
        ListeUserTicket[_ID_User][ID_Ticket].validity = _validity;
        ListeUserTicket[_ID_User][ID_Ticket].ticketCreator = _ticketCreator;
        ListeUserTicket[_ID_User][ID_Ticket].ticketOwner = _ticketOwner;
        ListeUserTicket[_ID_User][ID_Ticket].options = _options;


        NumberTickettUser[_ID_User] = ID_Ticket;
        
        ID_Ticket++;
  
    }
    
      function  getUserPro (uint ID_UserPro) view public returns ( string memory ,
        string memory ,
        string  memory ,
        string memory ,
        string memory ,
        string memory ,
        address ) {
          return(   
        ListProUser[ID_UserPro].Companyname ,
        ListProUser[ID_UserPro].Companyemail,
        ListProUser[ID_UserPro].Identification ,
        ListProUser[ID_UserPro].CompanyAddress ,
        ListProUser[ID_UserPro].CompanyArea ,
        ListProUser[ID_UserPro].CompanyBankAccount,
                ListProUser[ID_UserPro].ProWallet


        );
               
      }

         function  getTicketUser (uint ID_User, uint _ID_Ticket) view public returns (
        string memory ,
        uint ,
        string memory ,
        bool ,
        uint,
        string memory 
        ) {
          return(   
         ListeUserTicket[ID_User][_ID_Ticket].TicketName,
        ListeUserTicket[ID_User][_ID_Ticket].TicketPrice,
        ListeUserTicket[ID_User][_ID_Ticket].TicketDescription,
        ListeUserTicket[ID_User][_ID_Ticket].validity,
        ListeUserTicket[ID_User][_ID_Ticket].ticketCreator,
        ListeUserTicket[ID_User][_ID_Ticket].options 

        );
              
      }
     
     
 }
 
